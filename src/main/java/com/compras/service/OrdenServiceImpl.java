package com.compras.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compras.dao.OrdenDao;
import com.compras.model.Orden;

@Service
public class OrdenServiceImpl implements OrdenService{
	@Autowired
	OrdenDao orderdao;

	@Override
	public Orden saveOrden(Orden orden) {
		return orderdao.save(orden);
	}
	
}
