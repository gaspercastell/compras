package com.compras.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compras.model.Orden;

public interface OrdenDao extends JpaRepository <Orden,Long>{
	
}
