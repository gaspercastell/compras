package com.compras.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compras.service.OrdenService;
import com.compras.model.Orden;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@RestController
@RequestMapping("/orden")
public class OrdenController {
	
	@Autowired
	OrdenService ordenservice;
	
	@PostMapping("/crear")
	ResponseEntity<Orden> crearOrden (@RequestBody Orden orden){
		try {
			return new ResponseEntity<>(ordenservice.saveOrden(orden),HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
